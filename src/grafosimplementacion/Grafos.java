
package grafosimplementacion;

/**
 *
 * @author ameri
 */
public class Grafos {
    public static Graph getCities(){
        Nodo pal = new Nodo("Palizada");
        Nodo cc = new Nodo("Ciudad del Carmen");
        Nodo esc = new Nodo("Escarcega");
        Nodo cam = new Nodo("Campeche");
        Nodo vill = new Nodo("Villahermosa");
        Nodo pale = new Nodo("Palenque");
        Nodo mam = new Nodo("Mamantel");
        Nodo cham = new Nodo("Champoton");
        

        
        
        pal.addEdge(new Edge(pal, cc, 227));
        pal.addEdge(new Edge(pal, esc, 211));
        pal.addEdge(new Edge(pal, cam, 361));
        pal.addEdge(new Edge(pal, vill, 150));
        pal.addEdge(new Edge(pal, pale, 118));
        pal.addEdge(new Edge(pal, mam, 168));
        pal.addEdge(new Edge(pal, cham, 294));
 
        Graph graph = new Graph();
        graph.addNodo(pal);
        graph.addNodo(cc);
        graph.addNodo(esc);
        graph.addNodo(cam);
        graph.addNodo(vill);
        graph.addNodo(pale);
        graph.addNodo(mam);
        graph.addNodo(cham);

        return graph;

 
    }
            
            
    public static void main(String[] args) {
        
        Graph graph = getCities();
        System.out.println(graph);
    }
    
}

