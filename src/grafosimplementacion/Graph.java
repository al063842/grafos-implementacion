
package grafosimplementacion;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author ameri
 */
public class Graph {
  private List<Nodo> nodos;

    public void addNodo(Nodo node) {
        if (nodos == null) {
            nodos = new ArrayList<>();
        }
        nodos.add(node);
    }
    
    public List<Nodo> getNodos() {
        return nodos;
    }

    @Override
    public String toString() {
        return "Graph{" + "nodos=" + nodos + '}';
    }

}

